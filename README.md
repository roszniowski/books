# Books #

This project was prepared as an recruitment task. 
Project is divided on backend(PHP/Symfony) and frontend(Angular10) part.

### How do I get set up? ###

In order to set up the frontend please:
* move to the frontend directory
* in command line: npm i && npm start

All of the modules should be installed and the client-side application should be running.

In order to run backend, REST API please:
* move to the backend directory
* to create local database: php bin/console doctrine:database:create
* after db creation proceed with launching migrations: php bin/console doctrine:migrations:migrate
* in command line: symfony server:start

### Creator and repo owner ###

Radosław Roszniowski
* email: radoslaw.roszniowski@gmail.com
* linkedin: https://www.linkedin.com/in/rados%C5%82aw-roszniowski-b12612144/