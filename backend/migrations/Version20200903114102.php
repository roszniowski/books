<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200903114102 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
        INSERT INTO category VALUES (1, \'Podręczniki\');
        INSERT INTO category VALUES (2, \'Fantasy\');
        INSERT INTO category VALUES (3, \'Science-Fiction\');
        INSERT INTO category VALUES (4, \'Przygodowe\');
        INSERT INTO category VALUES (5, \'Obyczajowe\');
        INSERT INTO category VALUES (6, \'Kryminał\');');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('CREATE SCHEMA public');
    }
}
