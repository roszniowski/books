<?php

namespace App\Repository;

use App\Entity\Book;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Book|null find($id, $lockMode = null, $lockVersion = null)
 * @method Book|null findOneBy(array $criteria, array $orderBy = null)
 * @method Book[]    findAll()
 * @method Book[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookRepository extends ServiceEntityRepository
{
    private $em;

    /**
     * BookRepository constructor.
     * @param ManagerRegistry $registry
     * @param EntityManagerInterface $em
     */
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $em)
    {
        parent::__construct($registry, Book::class);
        $this->em = $em;
    }

    public function save(Book $book)
    {
        $this->em->persist($book);
        $this->em->flush();
        return $book;
    }

    public function update(Book $book)
    {
        $this->em->persist($book);
        $this->em->flush();
        return $book;
    }

    public function remove(int $id)
    {
        $book = $this->em->getReference(Book::class, $id);
        $title = $book->getTitle();
        $this->em->remove($book);
        $this->em->flush();
        return $title;
    }
}
