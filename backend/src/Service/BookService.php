<?php

namespace App\Service;

use App\Entity\Book;
use App\Entity\Category;
use App\Repository\BookRepository;
use Doctrine\ORM\EntityManagerInterface;

class BookService
{
    private $em;
    private $bookRepository;

    /**
     * BookService constructor.
     * @param EntityManagerInterface $em
     * @param BookRepository $bookRepository
     */
    public function __construct(EntityManagerInterface $em, BookRepository $bookRepository)
    {
        $this->em = $em;
        $this->bookRepository = $bookRepository;
    }


    public function save(array $book)
    {
        $newEntry = new Book();
        $newEntry->setTitle($book['title']);
        $newEntry->setAuthor($book['author']);
        $newEntry->setCategory($this->em->getReference(Category::class, $book['category']));
        $newEntry->setPublisher($book['publisher']);

        return $this->bookRepository->save($newEntry);
    }

    public function update(array $book)
    {
        $entry = $this->bookRepository->find($book['id']);
        $entry->setTitle($book['title']);
        $entry->setAuthor($book['author']);
        $entry->setCategory($this->em->getReference(Category::class, $book['category']));
        $entry->setPublisher($book['publisher']);
        return $this->bookRepository->update($entry);
    }

    public function remove(int $id)
    {
        return $this->bookRepository->remove($id);
    }
}
