<?php

namespace App\Controller;

use App\Repository\BookRepository;
use App\Repository\CategoryRepository;
use App\Service\BookService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BooksController extends AbstractController
{
    private $bookRepository;
    private $categoryRepository;
    private $bookService;

    /**
     * BooksController constructor.
     * @param BookRepository $bookRepository
     * @param CategoryRepository $categoryRepository
     * @param BookService $bookService
     */
    public function __construct(BookRepository $bookRepository,
                                CategoryRepository $categoryRepository,
                                BookService $bookService)
    {
        $this->bookRepository = $bookRepository;
        $this->categoryRepository = $categoryRepository;
        $this->bookService = $bookService;
    }

    /**
     * @Route("/books", name="books_list", methods={"GET"})
     */
    public function getBooksAction()
    {
        return $this->json([
            'books' => $this->bookRepository->findAll(),
            'categories' => $this->categoryRepository->findAll()
        ]);
    }

    /**
     * @Route("/books", name="books_add", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function addAction(Request $request)
    {
        $params = $request->getContent();
        if (!isset($params)) {
            return $this->json([
                'message' => 'Wrong params for new book entry.'
            ], 400);
        }
        $book = $this->bookService->save(json_decode($params, true));
        return $this->json([
            'message' => $book->getTitle() . ' has been added to the book list.'
        ], 200);
    }

    /**
     * @Route("/books", name="books_update", methods={"PUT"})
     * @param Request $request
     * @return JsonResponse
     */
    public function updateAction(Request $request)
    {
        $params = $request->getContent();
        if (!isset($params)) {
            return $this->json([
                'message' => 'Wrong params for book update.'
            ], 400);
        }
        $this->bookService->update(json_decode($params, true));
        return $this->json([
            'message' => 'Book details has been updated.'
        ], 200);
    }

    /**
     * @Route("/books/{id}", name="books_remove", methods={"DELETE"})
     * @param $id
     * @return JsonResponse
     */
    public function removeAction(int $id)
    {
        if(!isset($id)){
            return $this->json([
                'message' => 'Provided book id does not exist in the database.'
            ], 400);
        }
        $book = $this->bookService->remove($id);
        return $this->json([
            'message' => '"'.$book.'" has been removed from the book list.'
        ], 200);
    }

}
