import {Component, OnInit} from '@angular/core';
import {IBook} from "./models/book.model";
import {BreakpointObserver, Breakpoints, BreakpointState} from "@angular/cdk/layout";
import {BooksService} from "./services/books.service";
import {MatDialog} from "@angular/material/dialog";
import {ICategory} from "./models/category.model";
import {BooksAddDialog} from "./modals/books-add/books-add.component";

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss']
})
export class BooksComponent implements OnInit {
  books: IBook[];
  categories: ICategory[];
  mobileView: boolean;
  isLoading = true;

  constructor(public _breakpointObserver: BreakpointObserver,
              private _booksService: BooksService,
              public _dialog: MatDialog) {
    _breakpointObserver
      .observe([Breakpoints.XSmall, Breakpoints.Small])
      .subscribe((state: BreakpointState) => {
        this.mobileView = state.matches;
      });
  }

  ngOnInit() {
    this.index();
  }

  index() {
    this._booksService.getBooksAndCategories().subscribe(
      next => {
        if (!next) return;
        let books = [];
        let categories = [];
        next['books'].forEach(book => {
          books.push(book);
        });
        next['categories'].forEach(category => {
          categories.push(category);
        });
        this.books = books.slice();
        this.categories = categories.slice();
        this.isLoading = false;
      },
      error => {
        this._booksService.openSnackBar(error.message);
        this.isLoading = false;
      }
    );
  }

  add() {
    const dialogRef = this._dialog.open(BooksAddDialog, {
      data: {categories: this.categories},
      width: '400px'
    });
    dialogRef.afterClosed().subscribe(
      newBookEntry => {
        if (!newBookEntry) return;
        this._booksService.addBook(newBookEntry).subscribe(
          next => {
            this.index();
            this._booksService.openSnackBar(next['message']);
          },
          error => {
            this.index();
            this._booksService.openSnackBar(error['message']);
          }
        );
      }
    );
  }

}
