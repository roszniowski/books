import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BooksComponent} from './books.component';
import {BooksListComponent} from './components/books-list/books-list.component';
import {BookComponent} from './components/books-list/components/book/book.component';
import {SharedModule} from "../../shared/shared.module";
import {BooksApiService} from "./services/books-api.service";
import {BooksService} from "./services/books.service";
import {BooksEditDialog} from './modals/books-edit/books-edit.component';
import {BooksAddDialog} from './modals/books-add/books-add.component';
import {BooksDetailsDialog} from './modals/books-details/books-details.component';

@NgModule({
  declarations: [
    BooksComponent,
    BooksListComponent,
    BookComponent,
    BooksEditDialog,
    BooksAddDialog,
    BooksDetailsDialog
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  providers: [
    BooksApiService,
    BooksService
  ]
})
export class BooksModule {
}
