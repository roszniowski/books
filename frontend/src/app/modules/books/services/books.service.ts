import {Injectable} from '@angular/core';
import {BooksApiService} from "./books-api.service";
import {catchError} from "rxjs/operators";
import {throwError} from "rxjs";
import {IBook} from "../models/book.model";
import {MatSnackBar} from "@angular/material/snack-bar";

@Injectable()
export class BooksService {

  constructor(
    private _booksApiService: BooksApiService,
    private matSnackBar: MatSnackBar) {
  }

  getBooksAndCategories() {
    return this._booksApiService.getBooksAndCategories().pipe(
      catchError(error => throwError(error.message))
    );
  }

  addBook(book: IBook) {
    return this._booksApiService.addBook(book).pipe(
      catchError(error => throwError(error.message))
    );
  }

  editBook(book: IBook) {
    return this._booksApiService.editBook(book).pipe(
      catchError(error => throwError(error.message))
    );
  }

  removeBook(bookId: number) {
    return this._booksApiService.removeBook(bookId).pipe(
      catchError(error => throwError(error.message))
    );
  }

  openSnackBar(message) {
    this.matSnackBar.open(message, '', {
      duration: 2000,
      horizontalPosition: 'center',
      panelClass: 'text-center'
    })
  }
}
