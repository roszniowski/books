import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {API} from "../../../../environments/environment";
import {IBook} from "../models/book.model";

@Injectable()
export class BooksApiService {

  constructor(private _http: HttpClient) {
  }

  getBooksAndCategories() {
    return this._http.get(API.url + '/books');
  }

  addBook(book: IBook) {
    return this._http.post(API.url + '/books', book);
  }

  editBook(book: IBook) {
    return this._http.put(API.url + '/books', book);
  }

  removeBook(bookId: number) {
    return this._http.delete(API.url + `/books/${bookId}`);
  }
}
