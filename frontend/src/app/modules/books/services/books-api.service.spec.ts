import {TestBed} from '@angular/core/testing';

import {BooksApiService} from './books-api.service';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {API} from "../../../../environments/environment";
import {IBook} from "../models/book.model";

describe('BooksApiService', () => {
  let service: BooksApiService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [BooksApiService]
    });
    service = TestBed.inject(BooksApiService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should retrieve all books and categories from the API using GET', () => {
    const dummyRequest = {
      books: [
        {
          id: 1,
          title: 'Wybuchowa ksiazka',
          author: 'John Rambo',
          category: {id: 7, name: 'Przygodowe'},
          publisher: 'Helion'
        },
        {
          id: 2,
          title: 'Harry Potter',
          author: 'J.K. Rowling',
          category: {id: 2, name: 'Fantasy'},
          publisher: 'Helion'
        }
      ],
      categories: [
        {id: 7, name: 'Przygodowe'},
        {id: 2, name: 'Fantasy'}
      ]
    };
    service.getBooksAndCategories().subscribe(data => {
      console.log(data);
      expect(data['books'].length).toBe(2);
      expect(data['categories'].length).toBe(2);
      expect(data).toEqual(dummyRequest);
    });
    const request = httpMock.expectOne(`${API.url}/books`);
    expect(request.request.method).toBe('GET');
    expect(request.request.responseType).toBe('json');
    request.flush(dummyRequest);
  });

  it('should create and return newly added book', () => {
    const dummyBook: IBook = {
      id: 1,
      title: 'Wybuchowa ksiazka',
      author: 'John Rambo',
      category: {id: 7, name: 'Przygodowe'},
      publisher: 'Helion'
    };
    service.addBook(dummyBook).subscribe(newBook => {
      expect(newBook).toBe(dummyBook);
    });
    const request = httpMock.expectOne(`${API.url}/books`);
    expect(request.cancelled).toBeFalsy();
    expect(request.request.method).toBe('POST');
    request.flush(dummyBook);
  });
});
