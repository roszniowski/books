export interface IBook {
  id: number,
  title: string,
  author: string,
  category: {
    id: number,
    name: string
  },
  publisher: string
}
