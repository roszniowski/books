import {Component, Inject} from '@angular/core';
import {IBook} from "../../models/book.model";
import {MAT_DIALOG_DATA} from "@angular/material/dialog";

@Component({
  selector: 'app-books-details',
  templateUrl: './books-details.component.html',
  styleUrls: ['./books-details.component.scss']
})
export class BooksDetailsDialog {
  book: IBook;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
    this.book = data;
  }

}
