import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {IBook} from "../../models/book.model";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ICategory} from "../../models/category.model";

@Component({
  selector: 'app-books-edit',
  templateUrl: './books-edit.component.html',
  styleUrls: ['./books-edit.component.scss']
})
export class BooksEditDialog implements OnInit {
  book: IBook;
  categories: ICategory[];
  bookForm: FormGroup;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private _fb: FormBuilder,
              private _dialogRef: MatDialogRef<BooksEditDialog>) {
    this.book = data.book;
    this.categories = data.categories;
  }

  ngOnInit() {
    this.bookForm = this._fb.group({
      id: new FormControl(this.book.id),
      title: new FormControl(this.book.title, [Validators.required, Validators.maxLength(50)]),
      author: new FormControl(this.book.author, [Validators.required, Validators.maxLength(25)]),
      category: new FormControl(this.book.category.id, [Validators.required]),
      publisher: new FormControl(this.book.publisher, [Validators.maxLength(25)])
    });
  }

  update() {
    this._dialogRef.close(this.bookForm.value);
  }

}
