import {Component, Inject, OnInit} from '@angular/core';
import {ICategory} from "../../models/category.model";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-books-add',
  templateUrl: './books-add.component.html',
  styleUrls: ['./books-add.component.scss']
})
export class BooksAddDialog implements OnInit {
  categories: ICategory[];
  bookForm: FormGroup;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private _dialogRef: MatDialogRef<BooksAddDialog>,
              private _fb: FormBuilder) {
    this.categories = data.categories;
  }

  ngOnInit() {
    this.bookForm = this._fb.group({
      title: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      author: new FormControl('', [Validators.required, Validators.maxLength(25)]),
      category: new FormControl('', [Validators.required]),
      publisher: new FormControl('', [Validators.required, Validators.maxLength(25)])
    });
  }

  add() {
    this._dialogRef.close(this.bookForm.value);
  }

}
