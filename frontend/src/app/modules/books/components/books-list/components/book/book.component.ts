import {Component, EventEmitter, Input, Output} from '@angular/core';
import {IBook} from "../../../../models/book.model";
import {BooksService} from "../../../../services/books.service";
import {MatDialog} from "@angular/material/dialog";
import {BooksDetailsDialog} from "../../../../modals/books-details/books-details.component";
import {BooksEditDialog} from "../../../../modals/books-edit/books-edit.component";
import {ICategory} from "../../../../models/category.model";

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss']
})
export class BookComponent {
  @Input() book: IBook;
  @Input() categories: ICategory[];
  @Input() mobileView: boolean;
  @Output() booksUpdate = new EventEmitter();

  constructor(private _booksService: BooksService,
              public _dialog: MatDialog) {
  }

  details() {
    this._dialog.open(BooksDetailsDialog, {
      data: this.book,
      width: '400px'
    });
  }

  edit() {
    const dialogRef = this._dialog.open(BooksEditDialog, {
      data: {book: this.book, categories: this.categories},
      width: '400px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result || result === this.book) return;
      this._booksService.editBook(result).subscribe(
        next => {
          this.booksUpdate.emit();
          this._booksService.openSnackBar(next['message']);
        }
      );
    });
  }

  remove() {
    this._booksService.removeBook(this.book.id).subscribe(
      next => {
        this.booksUpdate.emit();
        this._booksService.openSnackBar(next['message']);
      }
    );
  }

}
