import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IBook} from "../../models/book.model";
import {ICategory} from "../../models/category.model";

@Component({
  selector: 'app-books-list',
  templateUrl: './books-list.component.html',
  styleUrls: ['./books-list.component.scss']
})
export class BooksListComponent {
  @Input() books: IBook[];
  @Input() categories: ICategory[];
  @Input() mobileView: boolean;
  @Output() booksUpdate = new EventEmitter();

  onBookRemove() {
    this.booksUpdate.emit();
  }
}
